package com.softwaretesting.testing.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class DebugFactorialTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    @DisplayName("n-1 factorial")
    void factorial_returnsFactorial() {
        assertEquals(6, DebugFactorial.factorial(4));
    }

    @Test
    @DisplayName("n factorial")
    void factorial_returnsWrongFactorial() {
        // this is actually right factorial
        assertNotEquals(24, DebugFactorial.factorial(4));
    }

    @Test
    @DisplayName("Main test")
    void mainTest_returnsResult() {
        DebugFactorial.main(null);
        assertEquals("Factorial of 5 is 24\r\n", outContent.toString());
//        \r\n instead of \n because of CRLF
    }

    @Test
    @DisplayName("Main test error")
    void mainTest_returnsError() {
        DebugFactorial.main(null);
        assertEquals("", errContent.toString());
    }
}