package com.softwaretesting.testing.validator;

import org.springframework.stereotype.Service;

@Service
public class PhoneNumberValidator {

    public boolean validatePhoneNumber(String phoneNumber) {
        String regex = "^(\\+49|0)\\d{8,11}";

        if (!phoneNumber.matches(regex)) {
            throw new RuntimeException("Given Phone Number is not valid for Germany.");
        }
        return true;
    }
}
