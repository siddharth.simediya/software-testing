package com.softwaretesting.testing.validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class CustomerValidatorTest {

    @Mock
    private CustomerValidator validator;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        validator = new CustomerValidator();
    }


    @Test
    @DisplayName("Presence Validation of 404 Object")
    void validate404Available() {
        // Create a mock optional object with a value
        Optional<String> optionalObject = Optional.of("Sid");

        // Call the validate404 method
        validator.validate404(optionalObject, "Label", "Value");
    }

    @Test
    @DisplayName("Absence Validation of 404 Object")
    void validate404NotAvailable() {
        // Create a mock optional object with no value
        Optional<String> optionalObject = Optional.empty();

        // Call the validate404 method and expect an exception to be thrown
        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> validator.validate404(optionalObject, "Label", "Value"));

        // Verify the exception status and reason
        assertEquals(HttpStatus.NOT_FOUND, exception.getStatus());
        assertEquals("java.util.Optional with Label'Value' does not exist.", exception.getReason());
    }

}
