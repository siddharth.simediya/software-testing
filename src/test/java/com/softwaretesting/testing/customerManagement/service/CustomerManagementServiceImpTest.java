package com.softwaretesting.testing.customerManagement.service;

import com.softwaretesting.testing.dao.CustomerRepository;
import com.softwaretesting.testing.exception.BadRequestException;
import com.softwaretesting.testing.exception.CustomerNotFoundException;
import com.softwaretesting.testing.model.Customer;
import com.softwaretesting.testing.validator.CustomerValidator;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.Array;
import java.util.*;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerManagementServiceImpTest {

    private final Customer customerA = new Customer(1011L, "user_a", "A", "+491515151515");

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerValidator customerValidator;

    @InjectMocks
    private CustomerManagementServiceImp underTest;

    @Test
    @DisplayName("Customer collection list")
    public void list_providesExpectedResult() {
        Customer a = new Customer(1L, "a", "A", "+4912345678");
        Customer b = new Customer(2L, "b", "B", "+49123456789");
        Customer c = new Customer(3L, "c", "C", "+491234567890");

        when(customerRepository.findAll()).thenReturn(Stream.of(a,b,c).collect(Collectors.toSet()));

        assertEquals(Stream.of(a,b,c).collect(Collectors.toSet()), underTest.list());
    }

    @Test
    @DisplayName("Customer with given username does not exist.")
    public void findByUserName_throwsException() {
        doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer with given username does not exist", null))
                .when(customerValidator).validate404(any(), anyString(), anyString());
        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> underTest.findByUserName("user"));
        assertEquals("Customer with given username does not exist", exception.getReason());
    }

    @Test
    @DisplayName("Returns customer with given username.")
    public void findByUserName_providesExpectedResult() {
        when(customerRepository.findByUserName(anyString())).thenReturn(Optional.of(customerA));
        doNothing().when(customerValidator).validate404(any(), anyString(), anyString());
        assertEquals(customerA, underTest.findByUserName("user"));
    }

    @Test
    @DisplayName("Customer with given id does not exist.")
    public void findById_throwsException() {
        doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer with given id does not exist", null))
                .when(customerValidator).validate404(any(), anyString(), anyString());
        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> underTest.findById(101L));
        assertEquals("Customer with given id does not exist", exception.getReason());
    }

    @Test
    @DisplayName("Returns customer with given id.")
    public void findById_providesExpectedResult() {
        when(customerRepository.findById(anyLong())).thenReturn(Optional.of(customerA));
        doNothing().when(customerValidator).validate404(any(), anyString(), anyString());
        assertEquals(customerA, underTest.findById(101L));
    }

    @Test
    @DisplayName("Customer with given phone number does not exist.")
    public void selectCustomerByPhoneNumber_throwsException() {
        doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer with given phone number does not exist", null))
                .when(customerValidator).validate404(any(), anyString(), anyString());
        ResponseStatusException exception = assertThrows(ResponseStatusException.class,
                () -> underTest.selectCustomerByPhoneNumber("+491515151515"));
        assertEquals("Customer with given phone number does not exist", exception.getReason());
    }

    @Test
    @DisplayName("Returns customer with given phone number.")
    public void selectCustomerByPhoneNumber_providesExpectedResult() {
        when(customerRepository.selectCustomerByPhoneNumber(anyString())).thenReturn(Optional.of(customerA));
        doNothing().when(customerValidator).validate404(any(), anyString(), anyString());
        assertEquals(customerA, underTest.selectCustomerByPhoneNumber("+491515151515"));
    }

    @Test
    @DisplayName("Customer Deletion failed.")
    public void delete_throwsException() {
        when(customerRepository.existsById(anyLong())).thenReturn(false);
        CustomerNotFoundException exception = assertThrows(CustomerNotFoundException.class,
                () -> underTest.delete(101L));
        assertEquals("Customer with id " + 101L + " does not exists", exception.getMessage());
    }

    @Test
    @DisplayName("Customer deleted successfully.")
    public void delete_providesExpectedResult() {
        when(customerRepository.existsById(anyLong())).thenReturn(true);
        underTest.delete(101L);
        verify(customerRepository, times(1)).deleteById(101L);
    }

    @Test
    @DisplayName("Customer Add Failed.")
    public void addCustomer_throwsException() {
        when(customerRepository.selectCustomerByPhoneNumber(anyString())).thenReturn(Optional.of(customerA));
        BadRequestException exception = assertThrows(BadRequestException.class,
                () -> underTest.addCustomer(customerA));
        assertEquals("Phone Number " + customerA.getPhoneNumber() + " taken", exception.getMessage());
    }

    @Test
    @DisplayName("Customer added successfully.")
    public void addCustomer_providesExpectedResult() {
        when(customerRepository.selectCustomerByPhoneNumber(anyString())).thenReturn(Optional.empty());
        underTest.addCustomer(customerA);
        verify(customerRepository, times(1)).save(customerA);
    }

    @Test
    @DisplayName("Customer mutation added successfully.")
    public void addCustomer_nullMutation_providesExpectedResult() {
        when(customerRepository.selectCustomerByPhoneNumber(anyString())).thenReturn(Optional.empty());
        when(customerRepository.save(customerA)).thenReturn(customerA);
        assertNotNull(underTest.addCustomer(customerA));
    }

    @Test
    @DisplayName("Customer collection list")
    public void saveAll_providesExpectedResult() {
        Customer a = new Customer(1L, "a", "A", "+4912345678");
        Customer b = new Customer(2L, "b", "B", "+49123456789");
        Customer c = new Customer(3L, "c", "C", "+491234567890");
        List<Customer> customers = Arrays.asList(a,b,c);

        when(customerRepository.saveAll(customers)).thenReturn(customers);

        Collection<Customer> result = underTest.saveAll(customers);
        assertTrue(customers.size() == result.size() && customers.containsAll(result) && customers.containsAll(result));
    }

}