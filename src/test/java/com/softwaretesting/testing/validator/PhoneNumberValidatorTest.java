package com.softwaretesting.testing.validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.*;

class PhoneNumberValidatorTest {

    private final PhoneNumberValidator underTest = new PhoneNumberValidator();

    @Test
    @DisplayName("Country Code for German Phone Number is present.")
    void germanCountryCodeValidatePhoneNumberTest() {
        String phoneNumber = "+4912345678";
        assertTrue(underTest.validatePhoneNumber(phoneNumber));
    }

    @Test
    @DisplayName("Leading zero for Local German Phone Number is present.")
    void leadingZeroValidatePhoneNumberTest() {
        String phoneNumber = "01234567890";
        assertTrue(underTest.validatePhoneNumber(phoneNumber));
    }

    @Test
    @DisplayName("Valid German Phone Number Length.")
    void validLengthPhoneNumberTest() {
        String phoneNumber = "012345678901";
        assertTrue(underTest.validatePhoneNumber(phoneNumber));
    }

    @Test
    @DisplayName("Country Codes other than Germany throws exception.")
    void otherCountryCodeValidatePhoneNumberTest() {
        String phoneNumber = "+512345678901";
        String expectedResult = "Given Phone Number is not valid for Germany.";
        RuntimeException exception = assertThrows(RuntimeException.class, () -> underTest.validatePhoneNumber(phoneNumber));
        assertEquals(expectedResult, exception.getMessage());
    }

    @Test
    @DisplayName("Absence of Country Code or leading zero throws exception.")
    void invalidStartOfPhoneNumberTest() {
        String phoneNumber = "123456789";
        String expectedResult = "Given Phone Number is not valid for Germany.";
        RuntimeException exception = assertThrows(RuntimeException.class, () -> underTest.validatePhoneNumber(phoneNumber));
        assertEquals(expectedResult, exception.getMessage());
    }

    @Test
    @DisplayName("Invalid German Phone Number Length throws exception.")
    void invalidLengthValidatePhoneNumberTest() {
        String phoneNumber = "+49123456";
        String expectedResult = "Given Phone Number is not valid for Germany.";
        RuntimeException exception = assertThrows(RuntimeException.class, () -> underTest.validatePhoneNumber(phoneNumber));
        assertEquals(expectedResult, exception.getMessage());
    }
}
