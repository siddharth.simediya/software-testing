package com.softwaretesting.testing.customerRegistration.controller;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class CustomerRegistrationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private final String urlTemplate = "http://localhost:8080/api/v1/customer-registration/";

    @Test
    void registrationCustomer() throws Exception {
        String body = "{ \"userName\": \"sid2412\", \"name\": \"sid\", \"phoneNumber\": \"+491638336639\" }";
        mockMvc.perform(post(urlTemplate)
                .contentType("application/json;charset=UTF-8").content(body))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
    }


    @Test
    void noContentRegister() throws Exception {
         mockMvc.perform(post(urlTemplate)
                .contentType("application/json;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.status().is(400)).andReturn();
    }

    @Test
    void notFoundCustomer() throws Exception {

        // Here Customer is not added
        String id = "2000";
        mockMvc.perform(get(urlTemplate + "/" + id)
                        .contentType("application/json;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.status().isNotFound()).andReturn();
    }
}
