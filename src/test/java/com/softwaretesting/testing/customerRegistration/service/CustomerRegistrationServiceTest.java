package com.softwaretesting.testing.customerRegistration.service;

import com.softwaretesting.testing.dao.CustomerRepository;
import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


class CustomerRegistrationServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    private CustomerRegistrationService customerRegistrationService;

    // Prepare test data
    private Customer customer;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        customerRegistrationService = new CustomerRegistrationService(customerRepository);
        // Prepare test data
        customer = new Customer();
        customer.setName("Siddharth");
        customer.setPhoneNumber("+491638336639");
    }

    @Test
    @DisplayName("Register New Customer - Success")
    void registerNewCustomerSuccess() {

        // Configure mock repository
        when(customerRepository.selectCustomerByPhoneNumber(any())).thenReturn(Optional.empty());
        when(customerRepository.save(any(Customer.class))).thenReturn(customer);

        // Perform registration
        Customer registeredCustomer = customerRegistrationService.registerNewCustomer(customer);

        // Verify interactions
        verify(customerRepository, times(1)).selectCustomerByPhoneNumber("+491638336639");
        verify(customerRepository, times(1)).save(customer);

        // Assert the result
        assertNotNull(registeredCustomer);
        assertEquals("Siddharth", registeredCustomer.getName());
        assertEquals("+491638336639", registeredCustomer.getPhoneNumber());
    }

    @Test
    @DisplayName("Register New Customer - Phone Number Already Taken")
    void registerNewCustomerPhoneNumberTaken() {
        Customer existingCustomer = new Customer();
        existingCustomer.setName("Siddharth S");
        existingCustomer.setPhoneNumber("+491638336639");

        when(customerRepository.selectCustomerByPhoneNumber(existingCustomer.getPhoneNumber()))
                .thenReturn(Optional.of(existingCustomer));

        Customer newCustomer = new Customer();
        newCustomer.setName("Siddharth S");
        newCustomer.setPhoneNumber("+491638336639");

        // Act & Assert
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            customerRegistrationService.registerNewCustomer(newCustomer);
        });

        assertEquals("You are already registered", exception.getMessage());
    }

    @Test
    @DisplayName("Register New Customer - Already Registered with Same Name")
    void registerNewCustomerAlreadyRegisteredWithSameName() {
        Customer existingCustomer = new Customer();
        existingCustomer.setName("Siddharth");
        existingCustomer.setPhoneNumber("+491234567890");

        // Configure mock repository
        when(customerRepository.selectCustomerByPhoneNumber(any())).thenReturn(Optional.of(existingCustomer));

        // Perform registration and assert exception
        assertThrows(IllegalStateException.class, () -> customerRegistrationService.registerNewCustomer(customer));

        // Verify interactions
        verify(customerRepository, times(1)).selectCustomerByPhoneNumber("+491638336639");
        verify(customerRepository, never()).save(any(Customer.class));
    }
}