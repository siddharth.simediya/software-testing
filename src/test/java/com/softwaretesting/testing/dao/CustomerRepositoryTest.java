package com.softwaretesting.testing.dao;

import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository instance;

    @BeforeEach
    void dataEntry() {
        Customer cust1 = new Customer();
        cust1.setName("Siddharth Simediya");
        cust1.setUserName("Siddharth2412");
        cust1.setPhoneNumber("+491638336639");
        instance.save(cust1);

        Customer cust2 = new Customer();
        cust2.setName("Parth Sheta");
        cust2.setUserName("Parth123");
        cust2.setPhoneNumber("+491743882854");
        instance.save(cust2);
    }

    @Test
    @DisplayName("Check User Name")
    void checkUsername() {
        Optional<Customer> r1 = instance.findByUserName("Siddharth2412");
        assertFalse(r1.isEmpty());
        assertEquals("Siddharth2412", r1.get().getUserName());

        Optional<Customer> r2 = instance.findByUserName("Parth123");
        assertFalse(r2.isEmpty());
        assertEquals("Parth123", r2.get().getUserName());
    }

    @Test
    @DisplayName("Check Wrong User Name")
    void checkFaultyUsername() {
        Optional<Customer> result1 = instance.findByUserName("Abc123");
        assertFalse(result1.isPresent());

        result1 = instance.findByUserName("Def456");
        assertFalse(result1.isPresent());
    }

    @Test
    @DisplayName("Check using Phone Number")
    void checkPhoneNumber() {
        Optional<Customer> result = instance.selectCustomerByPhoneNumber("+491638336639");
        assertTrue(result.isPresent());
        assertEquals("Siddharth2412", result.get().getUserName());
    }

    @Test
    @DisplayName("Check using Phone Number")
    void checkFaultyPhoneNumber() {
        Optional<Customer> result = instance.selectCustomerByPhoneNumber("1234567890");
        assertFalse(result.isPresent());
    }

    @Test
    @DisplayName("Adding Customer")
    void addingCustomer() {
        assertEquals(2, instance.count());

        Customer cust = new Customer();
        cust.setName("Pranav Desai");
        cust.setUserName("PranavErorr");
        cust.setPhoneNumber("+4915202311411");
        instance.save(cust);

        assertEquals(3, instance.count());
    }

    @Test
    @DisplayName("Deleting Customer")
    void deletingCustomer() {
        assertEquals(2, instance.count());
        Optional<Customer> cust = instance.findByUserName("Parth123");
        assertTrue(cust.isPresent());
        assertEquals("Parth123", cust.get().getUserName());
        instance.delete(cust.get());
        assertEquals(1, instance.count());
    }
}
