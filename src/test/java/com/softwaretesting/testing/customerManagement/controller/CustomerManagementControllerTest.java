package com.softwaretesting.testing.customerManagement.controller;

import com.softwaretesting.testing.customerManagement.service.CustomerManagementService;
import com.softwaretesting.testing.customerRegistration.service.CustomerRegistrationService;
import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class CustomerManagementControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerManagementService customerManagementService;
    @MockBean
    private CustomerRegistrationService customerRegistrationService;

    Customer a = new Customer(1L    , "a", "A", "+4912345678");
    Customer b = new Customer(2L, "b", "B", "+49123456789");
    Customer c = new Customer(3L, "c", "C", "+491234567890");

    private final String urlTemplate = "http://localhost:8080/api/v1/customers";

    @Test
    void listTest_providesExpectedResult() throws Exception {
        List<Customer> customers = Arrays.asList(a,b,c);

        when(customerManagementService.list()).thenReturn(customers);

        mockMvc.perform(MockMvcRequestBuilders.get(urlTemplate + "/list"))
                .andDo(print())
                .andExpect(status().isOk());
        verify(customerManagementService).list();
    }

    @Test
    void getByIdTest_providesCustomerWithGivenId() throws Exception {
        when(customerManagementService.findById(1L)).thenReturn(a);

        mockMvc.perform(MockMvcRequestBuilders.get(urlTemplate + "/1"))
                .andDo(print())
                .andExpect(status().isOk());
        verify(customerManagementService).findById(1L);
    }

    @Test
    void delete_providesExpectedResult() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete(urlTemplate + "/2"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}