package com.softwaretesting.testing.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Year;

import static org.junit.jupiter.api.Assertions.*;

class MiscTest {

    @Test
    @DisplayName("Class Test")
    void classCreator() {
        Misc misc = new Misc();
        assertNotNull(misc);
    }

    @Test
    @DisplayName("Adding Positive Numbers")
    void addingPositiveNumbers() {
        assertEquals(8, Misc.sum(3, 5));
    }

    @Test
    @DisplayName("Adding Negative Numbers")
    void addingNegativeNumbers() {
        assertEquals(-7, Misc.sum(-3, -4));
    }

    @Test
    @DisplayName("Division by Zero")
    void divisionbyZero() {
        assertThrows(RuntimeException.class, () -> Misc.divide(5, 0));
    }

    @Test
    @DisplayName("Normal Division")
    void normalDivision() {
        assertEquals(2, Misc.divide(8, 4));
    }

    @Test
    @DisplayName("Division of friction")
    void divisiontoFriction() {
        assertEquals(5, Misc.divide(11, 2));
    }

    @Test
    @DisplayName("Null Color")
    void nullColor() {
        assertThrows(IllegalArgumentException.class, () -> Misc.isColorSupported(null));
    }

    @Test
    @DisplayName("Color Test Red")
    void colorTestRed() {
        assertTrue(Misc.isColorSupported(Misc.Color.RED));
    }

    @Test
    @DisplayName("Color Test Yellow")
    void colorTestYellow() {
        assertTrue(Misc.isColorSupported(Misc.Color.YELLOW));
    }

    @Test
    @DisplayName("Color Test Blue")
    void colorTestBlue() {
        assertTrue(Misc.isColorSupported(Misc.Color.BLUE));
    }

    @Test
    @DisplayName("False Factorial")
    void falseFactorial() {
        assertEquals(1, Misc.calculateFactorial(-1));
        assertEquals(1, Misc.calculateFactorial(0));
    }

    @Test
    @DisplayName("Factorial of 1")
    void factorialof1() {
        assertEquals(1, Misc.calculateFactorial(1));
    }

    @Test
    @DisplayName("Factorial of 10")
    void factorialof10() {
        assertEquals(3628800, Misc.calculateFactorial(10));
    }

    @Test
    @DisplayName("False Prime")
    void falsePrime() {
        assertFalse(Misc.isPrime(-2, 0));
        assertFalse(Misc.isPrime(0, 0));
    }

    @Test
    @DisplayName("False Prime for 1")
    void falsePrimefor1() {
        if (Year.now().isAfter(Year.of(1944))) {
            assertFalse(Misc.isPrime(1, 0));
        } else {
            // :D
            assertTrue(Misc.isPrime(1, 0));
        }
    }

    @Test
    @DisplayName("True Prime")
    void truePrime() {
        assertTrue(Misc.isPrime(2, 0));
    }

    @Test
    @DisplayName("Multiple False Prime")
    void multipleFalsePrime() {
        assertFalse(Misc.isPrime(10, 2));
    }

    @Test
    @DisplayName("Positive Even Check")
    void positiveEvenCheck() {
        assertTrue(Misc.isEven(12));
    }

    @Test
    @DisplayName("Negative Even Check")
    void negativeEvenCheck() {
        assertTrue(Misc.isEven(-12));
    }

    @Test
    @DisplayName("0 is Even Check")
    void isEvenCheck0() {
        assertTrue(Misc.isEven(0));
    }

    @Test
    @DisplayName("Positive Odd Check")
    void positiveOddCheck() {
        assertFalse(Misc.isEven(13));
    }

    @Test
    @DisplayName("Negative Odd Check")
    void negativeOddCheck() {
        assertFalse(Misc.isEven(-13));
    }
}
